#!/usr/bin/env zsh

# zsh is used because shebang line of wk-bg contains a relative path
# which does not work with sh(1) in FreeBSD

if ! [[ -f quicklisp.lisp ]]; then
    fetch https://beta.quicklisp.org/quicklisp.lisp
fi

if [[ -f quicklisp.lisp ]]; then
    QL_SHA256="4a7a5c2aebe0716417047854267397e24a44d0cce096127411e9ce9ccfeb2c17"
    if ! sha256 -c $QL_SHA256 quicklisp.lisp ; then
        rm -f quicklisp.lisp
        echo quicklisp.lisp failed to validate, or outdated checksum
        exit 1
    fi
fi

# export WANIKANI_TOKEN="YOUR WANIKANI TOKEN"
export WANIKANI_BG_WIDTH=1920
export WANIKANI_BG_HEIGHT=1080
export WANIKANI_BG_PATH=/tmp/wk-bg.svg
export WANIKANI_LU_FONT=Iosevka
export WANIKANI_LU_SIZE=10

exec ./wk-bg.lisp
