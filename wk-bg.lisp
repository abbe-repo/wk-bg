#!sbcl --script

;; Licensed under: Public Domain

;;
;; inspired from: https://github.com/dbowring/wanikani-wallpaper-js
;;

(if (probe-file #p"ql/setup.lisp")
    (load #p"ql/setup.lisp")
    (load #p"quicklisp.lisp"))

#-quicklisp
(progn
  (quicklisp-quickstart:install :path #p"ql/")
  (load #p"ql/setup.lisp"))

(progn
  (ql:update-client)
  (ql:update-all-dists)
  (ql:quickload '(:dexador
		  :alexandria
		  :yason
		  :ironclad ;; could use sha1, but conflicting dependency name: https://github.com/massung/sha1/pull/2
		  :babel
		  :iterate)))

(defpackage :wk-bg
  (:use :common-lisp :iterate)
  (:export :main))

(in-package :wk-bg)

(defparameter cache-dir 
  (merge-pathnames (make-pathname :directory '(:relative  "wk-bg"))
		   (merge-pathnames (make-pathname :directory '(:relative ".cache")) (user-homedir-pathname)))
  "Cache directory")

(defvar ca-certificate-path
  (or (uiop:getenv "CURL_CA_BUNDLE")
      (uiop:getenv "GIT_SSL_CAINFO")
      (let ((fbsd-ca-path #p"/etc/ssl/cert.pem")) ;; default certificate path on FreeBSD
	(when (probe-file fbsd-ca-path)
	  fbsd-ca-path))))

(defvar wanikani-token (uiop:getenv "WANIKANI_TOKEN")
  "WaniKani authentication token")

(defvar list-of-kanjis nil
  "List of Kanjis present in WaniKani system")

(defvar heisig-order nil
  "Kanji characters in Heisig order")

(defvar wanikani-bg-width 1920 "Wallpaper width")
(defvar wanikani-bg-height 1080 "Wallpaper height")
(defvar wanikani-bg-padding 40 "Wallpaper padding")
(defvar wanikani-bg-path "/tmp/wk-bg.svg" "Wallpaper path")
(defvar wanikani-last-updated-font "Iosevka" "Font for Last Updated text")
(defvar wanikani-last-updated-font-size 10 "Font size for Last Updated text in points")

(defun auth-headers nil
  (list (cons "user-agent" "wk-bg.lisp")
	(cons "wanikani-revision" "20170710")
	(cons "authorization" (format nil "Bearer ~a" wanikani-token))))

(defun wanikani-request (url)
  (when url
    (dex:get url 
	     :ca-path ca-certificate-path
	     :headers (auth-headers)
	     :verbose nil)))

(defmacro string-assoc (key alist)
  `(alexandria:assoc-value ,alist ,key :test #'string-equal))

(defun process-data (data)
  (if (null data)
      nil
      (let ((yason:*parse-object-as* :alist))
	(yason:parse data))))

(defun sha1-hex (buffer)
  (ironclad:byte-array-to-hex-string
   (ironclad:digest-sequence :sha1 (babel:string-to-octets buffer))))

(defun cache-data (url)
  (if (null url)
      nil
      (progn
	(unless (probe-file cache-dir)
	  (ensure-directories-exist cache-dir))
	(let* ((hash (sha1-hex url))
	       (hash-file (merge-pathnames (make-pathname :name hash)
					   cache-dir)))
	  (unless (probe-file hash-file)
	    (with-open-file (cache-out hash-file :direction :output :if-does-not-exist :create :external-format :utf-8)
	      (write-sequence (wanikani-request url) cache-out)))
	  (with-open-file (cache-in hash-file :direction :input :external-format :utf-8)
	    (let ((buffer (make-sequence 'string (file-length cache-in))))
	      (read-sequence buffer cache-in)
	      buffer))))))

(defun fetch-subjects ()
  (reduce (lambda (ht entry)
	    (setf (gethash (car entry) ht) (cdr entry))
	    ht)
	  (apply #'concatenate 'list
		 (iter
		  (for url initially "https://api.wanikani.com/v2/subjects"
                       then (string-assoc "next_url" (string-assoc "pages" data)))
		  (for data next (process-data (cache-data url)))
		  (while data)
		  (collect
		   (mapcar (lambda (el)
                             (cons (string-assoc "id" el)
				   (string-assoc "characters" (string-assoc "data" el))))
			   (delete-if-not (lambda (el)
                                            (string-equal "kanji" (string-assoc "object" el)))
					  (string-assoc "data" data))))))
	  :initial-value (make-hash-table)))

(defun fetch-assignments ()
  (reduce (lambda (ht entry)
	    (setf (gethash (aref (car entry) 0) ht) (cdr entry))
	    ht)
	  (mapcar (lambda (el)
		    (cons (gethash
			   (string-assoc "subject_id"
					 (string-assoc "data" el))
			   list-of-kanjis)
			  (string-assoc "srs_stage"
					(string-assoc "data" el))))
		  (delete-if-not (lambda (el)
				   (string-equal "kanji"
						 (string-assoc "subject_type"
							       (string-assoc "data" el))))
				 (apply #'concatenate 'list
					   (mapcar (lambda (lst)
						     (string-assoc "data" lst))
						   (iter
						    (for url initially "https://api.wanikani.com/v2/assignments?srs_stages=0,1,2,3,4,5,6,7,8,9"
							 then (string-assoc "next_url" (string-assoc "pages" data)))
						    (for data next (process-data (wanikani-request url)))
						    (while data)
						    (collect data))))))
	  :initial-value (make-hash-table)))

(defvar colors
  '(;; apprentice
    (0 . "rgb(255,0,0)")
    (1 . "rgb(148,0,96)")
    (2 . "rgb(176,0,115)")
    (3 . "rgb(201,0,131)")
    (4 . "rgb(221,0,147)")
    ;; guru
    (5 . "rgb(95,31,110)")
    (6 . "rgb(136,45,158)")
    ;; master
    (7 . "rgb(41,77,219)")
    ;; enlightened
    (8 . "rgb(0,147,221)")
    ;; burned
    (9 . "rgb(255,255,255)")))

(defun draw-svg (file width height padding list-of-assignments)
  (flet ((get-current-time nil
	   (multiple-value-bind (_s minute hour date month year _day _daylight _tz)
	       (get-decoded-time)
	     (declare (ignore _day _s _daylight _tz))
	     (format nil "~4,'0d-~2,'0d-~2,'0d ~2,'0d:~2,'0d" year month date hour minute))))
    (let* ((font-size (floor (- (sqrt (/ (* (- width (* 2 padding))
					    (- height (* 2 padding)))
                                         (length heisig-order)))
				0)))
	   (text-pos-x 35)
	   (text-pos-y (- height padding)))
      
      (with-open-file (out file
			   :direction :output
			   :if-exists :supersede
			   :if-does-not-exist :create)
	(write-line "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" out)
	(format out "<svg version=\"2.0\" width=\"~a\" height=\"~a\" font-size=\"~a\" xmlns=\"http://www.w3.org/2000/svg\">~%" width height font-size)
	(write-line "<rect width=\"100%\" height=\"100%\" fill-opacity=\"100%\" fill=\"black\" />" out)
	(format out "<text fill=\"white\" x=\"~a\" y=\"~a\" class=\"character\" font-family=\"~a\" font-size=\"~apt\" transform=\"rotate(270 ~a,~a)\">Last Updated: ~a</text>~%"
		text-pos-x text-pos-y
		wanikani-last-updated-font wanikani-last-updated-font-size
		text-pos-x text-pos-y (get-current-time))

	(iter
	  (for ch in-string heisig-order)
	  (for ht-entry next (gethash ch list-of-assignments))
	  (for text-color next (if ht-entry (alexandria:assoc-value colors ht-entry) "rgb(128,128,128)"))
          (with offset = (+ font-size 0))
          (with effective-width = (- width (* 1 padding)))
          (for x
               first padding
               then (if (>= (+ x offset) effective-width) padding (+ x offset)))
          (for y
               first (+ padding offset)
               then (if (= x padding) (+ y offset) y))
          (after-each
	   (format out "<rect x=\"~a\" y=\"~a\" fill-opacity=\"100%\" fill=\"none\" stroke=\"rgb(30,30,30)\" width=\"~a\" height=\"~a\"/>~%"
		   x (- y font-size) font-size font-size)
           (format out
                   "<text class=\"character\" fill=\"~a\" x=\"~a\" y=\"~a\" text-rendering=\"geometricPrecision\">~a</text>~%"
                   text-color x y ch)))
	(write-line "</svg>" out)))))

(defmacro read-from-env (env-var def-var &optional is-int)
  (let ((env-val (gensym)))
    `(let ((,env-val (uiop:getenv ,env-var)))
       (when ,env-val
	 (setf ,def-var
	       ,(if is-int
		     `(parse-integer ,env-val)
		     env-val))))))

(read-from-env "WANIKANI_BG_WIDTH"    wanikani-bg-width t)
(read-from-env "WANIKANI_BG_HEIGHT"   wanikani-bg-height t)
(read-from-env "WANIKANI_BG_PADDING"  wanikani-bg-padding t)
(read-from-env "WANIKANI_BG_PATH"     wanikani-bg-path)
(read-from-env "WANIKANI_LU_FONT"     wanikani-last-updated-font)
(read-from-env "WANIKANI_LU_FONTSIZE" wanikani-last-updated-font-size t)

(defun main ()
  (draw-svg wanikani-bg-path
            wanikani-bg-width wanikani-bg-height
            wanikani-bg-padding
            (fetch-assignments)))

(setf heisig-order
      (with-open-file (heisig-txt #p"heisig.txt" :direction :input :external-format :utf-8)
	(read-line heisig-txt)))

(unless wanikani-token
  (error "No WANIKANI_TOKEN defined."))

(setf list-of-kanjis (fetch-subjects))

#+sbcl
(sb-ext:save-lisp-and-die (merge-pathnames #p"bin/wk-bg"
					   (user-homedir-pathname))
			  :executable t
			  :toplevel #'wk-bg:main)

#+ccl
(ccl:save-application "wk-bg"
		      :native t :toplevel-function #'wk-bg:main)
